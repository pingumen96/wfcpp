#include <memory>
#include "Renderer.h"
#include "InputWFCBitmapObject.h"
#include "OutputWFCBitmapObject.h"
#include "Texture.h"
#include "Shader.h"
#include "VertexArray.h"
#include "WFCState.h"
#include "Application.h"

Renderer::Renderer(Application* app, float screenWidth, float screenHeight) :
	app(app),
	screenHeight(screenHeight),
	screenWidth(screenWidth),
	bitmaps(std::vector<WFC::InputWFCBitmapObject*>()),
	outputImage(nullptr),
	bitmapShader(nullptr),
	window(nullptr){
}

bool Renderer::initialize() {
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8); // 32 byte in tutto
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1); // attiva double-buffering
	SDL_GL_SetAttribute(SDL_GL_ACCELERATED_VISUAL, 1); // accelerazione hw

	window = SDL_CreateWindow("Ole", 100, 50, static_cast<int>(screenWidth), static_cast<int>(screenHeight), SDL_WINDOW_OPENGL);
	if(!window) {
		SDL_Log("Errore nella creazione della finestra: %s", SDL_GetError());
		return false;
	}

	context = SDL_GL_CreateContext(window);

	glewExperimental = GL_TRUE;

	if(glewInit() != GLEW_OK) {
		SDL_Log("Errore nell'inizializzare GLEW.");
		return false;
	}

	glGetError();

	if(!loadShaders()) {
		SDL_Log("Failed to load shaders.");
		return false;
	}

	createBitmapVerts();

	return true;
}

void Renderer::shutdown() {
	SDL_GL_DeleteContext(context);
	SDL_DestroyWindow(window);
}

void Renderer::draw() {
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glEnable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);

	// 3D rendering
	// verr� fatto pi� avanti nel progetto

	glDisable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendEquationSeparate(GL_FUNC_ADD, GL_FUNC_ADD);
	glBlendFuncSeparate(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, GL_ONE, GL_ZERO);

	// 2D rendering
	bitmapShader->setActive();
	bitmapVerts->SetActive();
	/*for(auto bitmap : bitmaps) {
		bitmap->draw(bitmapShader, 4.0f);
		//bitmap->drawTiles(bitmapShader);
	}*/

	outputImage->draw(bitmapShader, 5.0f);

	SDL_GL_SwapWindow(window);
}

void Renderer::setWFCState(WFC::WFCState * state) {
	bitmaps.clear();
	bitmaps.emplace_back(state->getInputBitmap());
	outputImage = state->getOutputBitmap();
}

MeshObject* Renderer::getMesh(const std::string& filename) {
	return nullptr;
}

void Renderer::createBitmapVerts() {
	float vertices[] = {
	-0.5f, 0.5f, 0.f, 0.f, 0.f, 0.0f, 0.f, 0.f, // top left
	0.5f, 0.5f, 0.f, 0.f, 0.f, 0.0f, 1.f, 0.f, // top right
	0.5f,-0.5f, 0.f, 0.f, 0.f, 0.0f, 1.f, 1.f, // bottom right
	-0.5f,-0.5f, 0.f, 0.f, 0.f, 0.0f, 0.f, 1.f  // bottom left
	};

	unsigned int indices[] = {
		0, 1, 2,
		2, 3, 0
	};

	bitmapVerts = new VertexArray(vertices, 4, VertexArray::PosNormTex, indices, 6);
}

bool Renderer::loadShaders() {
	// Create sprite shader
	bitmapShader = new Shader();
	if(!bitmapShader->load("./Shaders/Sprite.vert", "./Shaders/Sprite.frag")) {
		return false;
	}

	bitmapShader->setActive();
	// Set the view-projection matrix
	Matrix4 bitmapViewProj = Matrix4::CreateSimpleViewProj(screenWidth, screenHeight);
	bitmapShader->setMatrixUniform("uViewProj", bitmapViewProj);

	return true;
}
