#pragma once
#include <vector>
#include <tuple>
#include <set>
#include <map>
#include <boost/optional.hpp>
#include "Color.h"

namespace WFC {
	class WFCOutputPixel {
	public:
		WFCOutputPixel(class OutputWFCBitmapObject* owner, unsigned long time);

		const boost::optional<Color>& getColor();
		const boost::optional<Color>& getVisualizedColor();
		void reset();
		void setColor(Color color, std::vector<uint8_t>& image, int width, int height, unsigned long time);
		void setVisualizedColor(Color color, std::vector<uint8_t>& image, int width, int height);
		bool isSet() const;
		unsigned long getCreationTime() { return creationTime; }
	private:
		class OutputWFCBitmapObject* owner;

		boost::optional<Color> color;
		boost::optional<Color> visualizedColor;

		std::map<Color, unsigned int> superpositions;
		unsigned long creationTime;

		friend class OutputWFCBitmapObject;
	};
}