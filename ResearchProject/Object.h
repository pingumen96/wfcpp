#pragma once
#include "Math.h"
#include <memory>
#include <GL/glew.h>
#include <GL/GL.h>

class Object {
public:
	Object(class Application* app);
	virtual void draw(class Shader* shader) = 0;


	const Vector3& getPosition() const { return position; }
	void setPosition(const Vector3& position) { this->position = position; recomputeTransform = true; }
	float getScale() const { return scale; }
	void setScale(float scale) { this->scale = scale; recomputeTransform = true; }
	const Quaternion& getRotation() const { return rotation; }
	void setRotation(const Quaternion& rotation) { this->rotation = rotation; recomputeTransform = true; }

	void computeWorldTransform();
	const Matrix4& getWorldTransform() const { return worldTransform; }

protected:
	Matrix4 worldTransform;
	class Application* app;

private:
	Vector3 position;
	Quaternion rotation;
	float scale;
	bool recomputeTransform;
};

