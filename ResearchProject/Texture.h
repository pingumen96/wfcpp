#pragma once
#include <iostream>
#include <stdint.h>
#include <vector>
#include <SDL.h>
#include <SOIL/SOIL.h>
#include <GL/glew.h>
#include <GL/GL.h>

class Texture {
public:
	Texture();
	~Texture();

	bool load(const std::string& filename);
	bool load(const uint8_t* image, int width, int height, int channels);
	void unload();
	void createForRendering(int width, int height, unsigned int format, uint8_t* pixels = nullptr);

	void setActive(int index = 0);

	int getWidth() const { return width; }
	int getHeight() const { return height; }
	unsigned int getTextureID() const { return textureID; }
	unsigned short getChannels() const { return channels; }
	const std::vector<uint8_t>& getImage() const { return image; }
	const std::string& getFilename() const { return filename; }

private:
	std::string filename;
	std::vector<uint8_t> image;
	unsigned int textureID;
	int channels;
	int width;
	int height;
};

