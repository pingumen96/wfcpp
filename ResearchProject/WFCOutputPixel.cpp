#include "WFCOutputPixel.h"
#include "OutputWFCBitmapObject.h"
#include <exception>
#include <boost/optional.hpp>
#include <iostream>
#include <utility>
#include <any>

WFC::WFCOutputPixel::WFCOutputPixel(OutputWFCBitmapObject * owner, unsigned long time) :
	owner(owner),
	color(boost::none), 
	creationTime(time),
	visualizedColor(boost::none){
}

const boost::optional<Color>& WFC::WFCOutputPixel::getColor(){
	return color;
}

const boost::optional<Color> & WFC::WFCOutputPixel::getVisualizedColor() {
	return visualizedColor;
}


void WFC::WFCOutputPixel::reset() {
	color = boost::none;
	creationTime = 0;
	//superpositions.clear();
}

void WFC::WFCOutputPixel::setColor(Color color, std::vector<uint8_t>& image, int width, int height, unsigned long time) {
	this->color = color;
	this->creationTime = time;
	setVisualizedColor(color, image, width, height);
}

void WFC::WFCOutputPixel::setVisualizedColor(Color color, std::vector<uint8_t>& image, int width, int height) {
	this->visualizedColor = color;
	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, width, height, GL_RGBA, GL_UNSIGNED_BYTE, image.data());
}

bool WFC::WFCOutputPixel::isSet() const {
	return color.has_value();
}
