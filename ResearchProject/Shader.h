#pragma once
#include <GL/glew.h>
#include <string>
#include "Math.h"

class Shader {
public:
	Shader();
	~Shader();
	bool load(const std::string& vertName, const std::string& fragName);
	void unload();
	// Set this as the active shader program
	void setActive();
	// Sets a Matrix uniform
	void setMatrixUniform(const char* name, const Matrix4& matrix);
	// Sets an array of matrix uniforms
	void setMatrixUniforms(const char* name, Matrix4* matrices, unsigned count);
	// Sets a Vector3 uniform
	void setVectorUniform(const char* name, const Vector3& vector);
	void setVector2Uniform(const char* name, const Vector2& vector);
	// Sets a float uniform
	void setFloatUniform(const char* name, float value);
	// Sets an integer uniform
	void setIntUniform(const char* name, int value);
private:
	// Tries to compile the specified shader
	bool compileShader(const std::string& fileName,
		GLenum shaderType,
		GLuint& outShader);

	// Tests whether shader compiled successfully
	bool isCompiled(GLuint shader);
	// Tests whether vertex/fragment programs link
	bool isValidProgram();
private:
	// Store the shader object IDs
	GLuint vertexShader;
	GLuint fragShader;
	GLuint shaderProgram;
};