#pragma once
#include "MeshObject.h"
class QuadMeshObject : public MeshObject {
public:
	QuadMeshObject() = delete;
	QuadMeshObject(class Application* app);

	virtual bool load(const std::string& filename, class Application* application);
};

