#pragma once
#include "Object.h"
#include <vector>
#include <string>
class MeshObject : public Object {
public:
	MeshObject(class Application* app);

	virtual bool load(const std::string& filename, class Application* app);
	void unload();

	class VertexArray* getVertexArray() { return vertexArray; }


private:
	std::vector<Vector4> vertices;
	std::vector<GLushort> elements;
	std::vector<Vector3> normals;
	std::vector<class Texture*> textures;
	class VertexArray* vertexArray;
	std::string shaderName;
};

