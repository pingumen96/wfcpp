#include "Random.h"



Random::Random() {
	std::random_device rd;
	rng = std::mt19937(rd());
}


Random::~Random() {
}

uint32_t Random::getNext(uint32_t min, uint32_t max) {
	std::uniform_int_distribution<uint32_t> distribution(min, max);
	return distribution(rng);
}
