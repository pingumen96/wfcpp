#include <chrono>
#include "WFCState.h"
#include "InputWFCBitmapObject.h"
#include "OutputWFCBitmapObject.h"

WFC::WFCState::WFCState(class InputWFCBitmapObject* input, class OutputWFCBitmapObject* output) :
	inputBitmap(input),
	outputBitmap(output) {
}
