#pragma once
#include <stdint.h>
#include <vector>
#include <tuple>
#include <memory>
#include <GL/glew.h>
#include <GL/GL.h>
#include "Shader.h"
#include "Color.h"
#include "BitmapObject.h"
#include "WFCPattern.h"

namespace WFC {
	class InputWFCBitmapObject : public BitmapObject, public std::enable_shared_from_this<InputWFCBitmapObject> {
	public:
		InputWFCBitmapObject() = delete;
		InputWFCBitmapObject(class Application* app, const unsigned short tileSize, const std::string& filename, const bool evaluateRotations = false, const bool evaluateXReflection = false, const bool evaluateYReflection = false);
		~InputWFCBitmapObject();
		void drawTiles(Shader* shader);
		void setTexture(class Texture* texture);

		std::map<Color, unsigned int> const& getColorsFrequencyMap() const {
			return colorsFrequency;
		}

		std::vector<Color> getColors();

		unsigned short getTileSize() const { return tileSize; }
		std::map<WFCPattern, unsigned int>& getPatterns() {
			return patterns;
		}

		const Color& getAverageColor() {
			return averageColor;
		}

	private:
		std::map<WFCPattern, unsigned int> patterns;
		std::map<Color, unsigned int> colorsFrequency;
		bool evaluateRotations;
		bool evaluateXReflection;
		bool evaluateYReflection;
		unsigned short tileSize;

		Color averageColor;

		void addPattern(WFCPattern pattern);
	};
}

