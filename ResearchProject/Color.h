#pragma once

#include <stdint.h>
#include <map>

struct Color {
	uint8_t red;
	uint8_t green;
	uint8_t blue;
	uint8_t alpha;


	// costruttori e roba varia
	Color(const uint8_t red, const uint8_t green, const uint8_t blue, const uint8_t alpha) :
		red(red), green(green), blue(blue), alpha(alpha) {
	}

	Color(const uint8_t red, const uint8_t green, const uint8_t blue) :
		Color(red, green, blue, 255){
	}

	Color() : Color(255, 255, 255, 255){
	}

	bool operator==(const Color& rhs) const {
		return (red == rhs.red) && (green == rhs.green) && (blue == rhs.blue) && (alpha == rhs.alpha);
	}

	bool operator!=(const Color& rhs) const {
		return !((*this) == rhs);
	}

	friend bool operator<(const Color& l, const Color& r) {
		return (l.red + l.green + l.blue) < (r.red + r.green + r.blue);
	}

	friend bool operator>(const Color& l, const Color& r) {
		return r < l;
	}

	friend bool operator<=(const Color& l, const Color& r) {
		return !(l > r);
	}

	friend bool operator>=(const Color& l, const Color& r) {
		return !(l < r);
	}

	static Color averageColor(const std::map<Color, unsigned int>& colors);
};