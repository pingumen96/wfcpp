#include "Application.h"
#include <iostream>
#include <ctime>
#include <memory>

#undef main

Application* app;

int main() {
	// debug messages
	std::cout << "For now, just use PNG with 4 channels" << std::endl;

	Application* app = new Application();
	bool success = app->initialize();
	
	std::clock_t begin = std::clock();
	if(success) {
		app->runLoop();
	}
	std::clock_t end = std::clock();
	std::cout << (double(end) - double(begin)) / CLOCKS_PER_SEC;
	system("pause");
	app->shutdown();
	return 0;
}