#include <iterator>
#include <stack>
#include <boost/optional.hpp>
#include "Math.h"
#include "WFCPattern.h"
#include "OutputWFCBitmapObject.h"
#include "InputWFCBitmapObject.h"
#include "Color.h"

WFC::WFCPattern::WFCPattern() :
	id(0),
	owner(nullptr),
	edgeSize(0){
}

WFC::WFCPattern::WFCPattern(InputWFCBitmapObject* owner, unsigned short size, BitmapPosition position) :
	id(0),
	owner(owner),
	edgeSize(size) {
	pixels = std::vector<std::vector<uint8_t>>(size * size);
	std::vector<uint8_t> image = owner->getImage();
	bmp = std::vector<uint8_t>(size * size * 4);
	for(unsigned int i = 0; i < pixels.size(); i++) {
		pixels[i] = std::vector<uint8_t>(4);
		pixels[i][0] = image[((position.x + i % size) % owner->getWidth()) * owner->getChannels() + (position.y + i / size) * owner->getWidth() * owner->getChannels() + 0];
		pixels[i][1] = image[((position.x + i % size) % owner->getWidth()) * owner->getChannels() + (position.y + i / size) * owner->getWidth() * owner->getChannels() + 1];
		pixels[i][2] = image[((position.x + i % size) % owner->getWidth()) * owner->getChannels() + (position.y + i / size) * owner->getWidth() * owner->getChannels() + 2];
		pixels[i][3] = 255;

		bmp[i * 4 + 0] = pixels[i][0];
		bmp[i * 4 + 1] = pixels[i][1];
		bmp[i * 4 + 2] = pixels[i][2];
		bmp[i * 4 + 3] = 255;

		Color tmp = Color(pixels[i][0], pixels[i][1], pixels[i][2], 255);

		if(colorsFrequencies.find(tmp) == colorsFrequencies.end()) {
			colorsFrequencies[tmp] = 1;
		} else {
			colorsFrequencies[tmp]++;
		}
	}
	initializeGL();

}

WFC::WFCPattern::WFCPattern(InputWFCBitmapObject* owner, unsigned short size, std::vector<uint8_t>& bmp, std::vector<std::vector<uint8_t>>& pixels) :
	id(0),
	owner(owner),
	edgeSize(size),
	bmp(bmp),
	pixels(pixels) {
	initializeGL();
}

std::vector<Color> WFC::WFCPattern::getColors() const {
	std::vector<Color> colors;

	for(auto c : colorsFrequencies) {
		colors.emplace_back(c.first);
	}

	return colors;
}

Color WFC::WFCPattern::getColor(BitmapPosition position) const {
	return Color(pixels[BitmapPosition::positionToIndex(position, edgeSize)][0],
		pixels[BitmapPosition::positionToIndex(position, edgeSize)][1],
		pixels[BitmapPosition::positionToIndex(position, edgeSize)][2]);
}

Color WFC::WFCPattern::getColor(int x, int y) const {
	return getColor(BitmapPosition(x, y));
}

bool WFC::WFCPattern::doesFit(BitmapPosition minCorner, OutputWFCBitmapObject& output, boost::optional<BitmapPosition> position, boost::optional<Color&> color) const {
	// per ogni tile del pattern, si controlla se quello corrispondente nell'output alla posizione indicata
	// ha qualcosa di diverso

	for(unsigned int i = 0; i < pixels.size(); i++) {
		BitmapPosition patternPosition = BitmapPosition::indexToPosition(i, edgeSize);
		Color patternColor = getColor(patternPosition);
		BitmapPosition outputPosition(minCorner.x + patternPosition.x, minCorner.y + patternPosition.y);
		boost::optional<Color> outputColor;
		if(outputPosition == position) {
			outputColor = color;
		} else {
			outputColor = output.getColor(outputPosition.x, outputPosition.y, 4);
		}

		if(outputColor != boost::none && outputColor.value() != patternColor) {
			return false;
		}
	}

	return true;
}

void WFC::WFCPattern::getRotations(WFCPattern pattern, std::vector<WFCPattern>& container) {
	container = std::vector<WFCPattern>(3);
	for(int i = 0; i < 3; i++) {
		for(int x = 0; x < pattern.getSize() / 2; x++) {
			for(int y = x; y < pattern.getSize() - x - 1; y++) {
				int firstIndex = BitmapPosition::positionToIndex(x, y, pattern.getSize());
				int secondIndex = BitmapPosition::positionToIndex(pattern.getSize() - 1 - y, x, pattern.getSize());
				int thirdIndex = BitmapPosition::positionToIndex(pattern.getSize() - 1 - x, pattern.getSize() - 1 - y, pattern.getSize());
				int fourthIndex = BitmapPosition::positionToIndex(y, pattern.getSize() - 1 - x, pattern.getSize());

				std::vector<uint8_t> tempPixel;

				tempPixel = pattern.pixels[firstIndex];
				pattern.pixels[firstIndex] = pattern.pixels[secondIndex];
				pattern.pixels[secondIndex] = pattern.pixels[thirdIndex];
				pattern.pixels[thirdIndex] = pattern.pixels[fourthIndex];
				pattern.pixels[fourthIndex] = tempPixel;
			}
		}

		for(int j = 0; j < pattern.getSize() * pattern.getSize(); j++) {
			for(int k = 0; k < 4; k++) {
				pattern.bmp[j * 4 + k] = pattern.pixels[j][k];
			}
		}

		//WFCPattern newPattern = 

		container[i] = WFCPattern(owner, edgeSize, pattern.bmp, pattern.pixels);;
	}
}

WFC::WFCPattern WFC::WFCPattern::getReflection(WFCPattern pattern, Math::Axis axis) {
	std::stack<std::vector<uint8_t>> stack;
	if(axis == Math::Axis::X) {
		for(int y = 0; y < pattern.getSize(); y++) {
			for(int x = 0; x < pattern.getSize(); x++) {
				stack.push(pattern.pixels[BitmapPosition::positionToIndex(x, y, edgeSize)]);
			}

			for(int x = 0; !stack.empty(); x++) {
				pattern.pixels[BitmapPosition::positionToIndex(x, y, edgeSize)] = stack.top();
				stack.pop();
			}
		}
	} else if(axis == Math::Axis::Y){
		for(int x = 0; x < pattern.getSize(); x++) {
			for(int y = 0; y < pattern.getSize(); y++) {
				stack.push(pattern.pixels[BitmapPosition::positionToIndex(x, y, edgeSize)]);
			}

			for(int y = 0; !stack.empty(); y++) {
				pattern.pixels[BitmapPosition::positionToIndex(x, y, edgeSize)] = stack.top();
				stack.pop();
			}
		}
	}
	

	
	// si scrive tutto sul pattern giusto
	for(int i = 0; i < pattern.getSize() * pattern.getSize(); i++) {
		for(int j = 0; j < 4; j++) {
			pattern.bmp[i * 4 + j] = pattern.pixels[i][j];
		}
	}

	return WFCPattern(owner, edgeSize, pattern.bmp, pattern.pixels);
}

void WFC::WFCPattern::initializeGL() {
	bool existsAlready = false;
	for(auto p : owner->getPatterns()) {
		if(p.first.pixels == pixels) {
			id = p.first.id;
			existsAlready = true;
		}
	}

	if(!existsAlready) {
		// si prepara ci� che serve per OpenGL
		glGenTextures(1, &id);
		glBindTexture(GL_TEXTURE_2D, id);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, edgeSize, edgeSize, 0, GL_RGBA, GL_UNSIGNED_BYTE, bmp.data());
		glGenerateMipmap(GL_TEXTURE_2D);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

		if(GLEW_EXT_texture_filter_anisotropic) {
			GLfloat largest;
			glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &largest);
			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, largest);
		}
		glBindTexture(GL_TEXTURE_2D, 0);
	}
}
