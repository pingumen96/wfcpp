#pragma once
#include <memory>
#include "OutputWFCBitmapObject.h"
#include "WFCState.h"
#include "Random.h"

class Application : public std::enable_shared_from_this<Application> {
public:
	// singleton
	static Application& getInstance() {
		static Application instance;
		return instance;
	}
	Application(Application const&) = delete;
	void operator=(Application const&) = delete;
	// fine singleton

	Application();
	bool initialize();
	void shutdown();

	WFC::WFCState* getWFCState() { return wfcState; }

	void generateOutput();
	void runLoop();
	void processInput();
	void updateObjects();

	void loadData(); // metodo temporaneo, finch� non si creer� un'interfaccia a dovere



	// utilities
	std::unique_ptr<Random> random;

private:
	void handleKeyPress(int key);
	class Renderer* renderer;
	WFC::WFCState * wfcState;
	bool isRunning;

	bool isPaused;
	bool canPause;
	bool finished;
};

