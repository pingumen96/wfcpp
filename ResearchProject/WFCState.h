#pragma once
#include <random>
#include "InputWFCBitmapObject.h"
#include "OutputWFCBitmapObject.h"

namespace WFC {
	class WFCState {
	public:
		WFCState(class InputWFCBitmapObject* input, class OutputWFCBitmapObject* output);

		class InputWFCBitmapObject* getInputBitmap() { return inputBitmap; }
		class OutputWFCBitmapObject* getOutputBitmap() { return outputBitmap; }

	private:
		class InputWFCBitmapObject* inputBitmap;
		class OutputWFCBitmapObject* outputBitmap;
	};
}
