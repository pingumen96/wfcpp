#pragma once
#include "Object.h"
#include "Color.h"
#include "BitmapPosition.h"
#include "Shader.h"
#include "Texture.h"
#include <stdint.h>
#include <boost/optional.hpp>
#include <vector>
#include <memory>

namespace WFC {
	class BitmapObject : public Object {
	public:
		BitmapObject(class Application* app);
		~BitmapObject();

		virtual void draw(class Shader* shader) override;
		virtual void draw(class Shader* shader, float scaleFactor);
		void setTexture(class Texture* texture);

		unsigned short getChannels();

		boost::optional<Color> getColor(int x, int y, unsigned short channels) const;

		std::vector<uint8_t> getImage() { return texture->getImage(); }
		int getWidth() { return texture->getWidth(); }
		int getHeight()  { return texture->getHeight(); }

		static void setColor(std::vector<uint8_t>& image, int h, int v, unsigned int imgWidth, unsigned int imgHeight, unsigned short channels, struct Color color);
		static Color discoverColor(const std::vector<uint8_t>& image, const int h, const int v, unsigned int imgWidth, unsigned short channels);
		static void fillWithColor(std::vector<uint8_t>& image, size_t imgWidth, size_t imgHeight, unsigned short channels, struct Color color);
	protected:
		class Texture* texture;

	private:
		int width;
		int height;
	};
}
