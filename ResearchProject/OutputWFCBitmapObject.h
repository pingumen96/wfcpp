#pragma once
#include <stdint.h>
#include <memory>
#include "Color.h"
#include "BitmapObject.h"
#include "InputWFCBitmapObject.h"
#include "WFCOutputPixel.h"

namespace WFC {
	class OutputWFCBitmapObject : public BitmapObject {
	public:
		static const Color clearedAreaColor;
		enum class ComputationState { RUNNING, CONTRADICTION, FINISHED };

		OutputWFCBitmapObject(class Application* app, class InputWFCBitmapObject& input, int height, int width, int clearSize = 6, bool xWrapping = false, bool yWrapping = false, bool oldPixelsFirst = false, bool randomClearSize = false);
		int getHeight() const { return height; }
		int getWidth() const { return width; }
		void saveImage(const std::string& filename);
		const ComputationState getCurrentState() const { return currentState; }

		// la roba che interessa davvero
		void reset();
		void observe();

		bool isSet(const BitmapPosition& position) const;
		std::vector<uint8_t> getImage() { return image; }

		boost::optional<Color> getColor(int x, int y, unsigned short channels);

		void draw(Shader* shader, float scaleFactor) override;

	private:
		InputWFCBitmapObject input;
		int height;
		int width;
		int clearSize;
		int currentClearSize;

		// useful parameters
		bool xWrapping;
		bool yWrapping;

		std::vector<uint8_t> image;
		std::vector<WFCOutputPixel> pixels;

		void setColor(Color color, int x, int y);
		void clearArea(BitmapPosition startPoint, int size);

		BitmapPosition lastModifiedPixelPosition;
		ComputationState currentState;

		// metodi di aiuto
		void selectLowestEntropyPixels(std::vector<BitmapPosition>& emptyVector);
		void resetAtPosition(BitmapPosition position);
		void recalculateSuperpositions(BitmapPosition startPoint, BitmapPosition endPoint);
		WFCOutputPixel& getPixel(const size_t x, const size_t y);

		unsigned long time;
		bool oldPixelsFirst;
		bool randomClearSize;
	};
}

