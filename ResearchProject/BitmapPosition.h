#pragma once
namespace WFC {
	struct BitmapPosition {
		int x;
		int y;

		BitmapPosition() :
			x(0),
			y(0) {
		}

		BitmapPosition(int x, int y) {
			this->x = x;
			this->y = y;
		}

		friend bool operator<(const BitmapPosition& l, const BitmapPosition& r) {
			return l.y < r.y&& l.x < r.x;
		}

		friend bool operator>(const BitmapPosition& l, const BitmapPosition& r) {
			return r < l;
		}

		friend bool operator<=(const BitmapPosition& l, const BitmapPosition& r) {
			return !(l > r);
		}

		friend bool operator>=(const BitmapPosition& l, const BitmapPosition& r) {
			return !(l < r);
		}

		friend inline bool operator==(const BitmapPosition& lhs, const BitmapPosition& rhs) {
			return lhs.x == rhs.x && lhs.y == rhs.y;
		}

		friend inline bool operator!=(const BitmapPosition& lhs, const BitmapPosition& rhs) {
			return !(lhs == rhs);
		}

		// statics
		inline static int positionToIndex(const BitmapPosition & position, const int& width) {
			return position.y* width + (position.x % width);
		}

		inline static int positionToIndex(const int& x, const int& y, const int& width) {
			return positionToIndex(BitmapPosition(x, y), width);
		}

		inline static BitmapPosition indexToPosition(const int& index, const int& width) {
			return BitmapPosition(index % width, index / width);
		}
	};
}
