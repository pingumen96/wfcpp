#pragma once
#include <random>
class Random {
public:
	Random();
	~Random();

	uint32_t getNext(uint32_t min, uint32_t max);

private:
	std::mt19937 rng;
};

