#include <stdint.h>
#include <string>
#include <tuple>
#include <vector>
#include <memory>
#include <algorithm>
#include <assert.h>
#include <limits>
#include <numeric>
#include <boost/range/numeric.hpp>
#include <boost/range/adaptor/map.hpp>
#include <set>
#include <map>
#include <SOIL/SOIL.h>
#include "BitmapObject.h"
#include "Texture.h"
#include "Application.h"
#include "Color.h"
#include "OutputWFCBitmapObject.h"
#include "InputWFCBitmapObject.h"
#include "WFCState.h"
#include "WFCOutputPixel.h"
#include "Random.h"

WFC::OutputWFCBitmapObject::OutputWFCBitmapObject(class Application* app, InputWFCBitmapObject& input, int height, int width, int clearSize, bool xWrapping, bool yWrapping, bool oldPixelsFirst, bool randomClearSize) :
	BitmapObject(app),
	time(0),
	height(height),
	width(width),
	oldPixelsFirst(oldPixelsFirst),
	randomClearSize(randomClearSize),
	input(input),
	xWrapping(xWrapping),
	yWrapping(yWrapping),
	clearSize(clearSize),
	currentClearSize(clearSize),
	currentState(WFC::OutputWFCBitmapObject::ComputationState::RUNNING) {
	for(int i = 0; i < height * width; i++) {
		pixels.emplace_back(WFCOutputPixel(this, 0));
	}

	size_t imageSize = height * width;
	image = std::vector<uint8_t>(imageSize * input.getChannels());

	for(auto p : pixels) {
		p.setVisualizedColor(input.getAverageColor(), image, width, height);
	}

	texture = new Texture();
	texture->load(image.data(), width, height, 4);
	reset();
}

void WFC::OutputWFCBitmapObject::saveImage(const std::string& filename) {
	SOIL_save_image(filename.c_str(), SOIL_SAVE_TYPE_BMP, height, width, 4, image.data());
}

void WFC::OutputWFCBitmapObject::reset() {
	/*for(int i = 0; i < height * width; i++) {
		pixels[i].reset();
		for(auto cf : input.getColorsFrequencyMap()) {
			pixels[i].superpositions[cf.first] = cf.second;
		}
		pixels[i].visualizedColor = input.getAverageColor();
	}*/

	recalculateSuperpositions(BitmapPosition(0, 0), BitmapPosition(width - 1, height - 1));
}

// it returns false when it finishes
void WFC::OutputWFCBitmapObject::observe() {
	time++;

	std::vector<BitmapPosition> lowEntropyPixels;
	selectLowestEntropyPixels(lowEntropyPixels);

	// se non ce ne sono significa che si ha finito
	if(lowEntropyPixels.size() == 0) {
		std::cout << "Finished!" << std::endl;
		currentState = ComputationState::FINISHED;
		return;
	}


	// va gestito il caso in cui non � possibile scegliere colori e dunque � impossibile proseguire
	if(getPixel(lowEntropyPixels[0].x, lowEntropyPixels[0].y).superpositions.size() == 0) {
		//std::cout << "Contradiction!" << std::endl;
		if(randomClearSize) {
			clearArea(lastModifiedPixelPosition, app->random->getNext(clearSize / 2, clearSize + clearSize / 2));
		} else {
			clearArea(lastModifiedPixelPosition, clearSize);
		}
		currentState = ComputationState::CONTRADICTION;

		return;
	}

	// scelta del pixel
	size_t randomPixelIndex = app->random->getNext(0, lowEntropyPixels.size() - 1);
	WFCOutputPixel pixel = getPixel(lowEntropyPixels[randomPixelIndex].x, lowEntropyPixels[randomPixelIndex].y);

	// scelta del colore tramite media pesata
	Color chosenColor;


	// inizio calcolo media pesata
	int totalFrequencies = boost::accumulate(pixel.superpositions | boost::adaptors::map_values, 0);
	int chosenIndex = app->random->getNext(0, totalFrequencies - 1);

	// si usa vector in quanto pi� facile da maneggiare
	std::vector<std::pair<Color, unsigned int>> colorFrequenciesVector;
	for(std::pair<Color, unsigned int> entry : pixel.superpositions) {
		colorFrequenciesVector.emplace_back(entry);
	}
	int colorIndex = 0;
	while(chosenIndex >= 0) {
		chosenIndex -= colorFrequenciesVector[colorIndex].second;
		colorIndex += 1;
	}
	colorIndex -= 1;
	chosenColor = colorFrequenciesVector[colorIndex].first;

	setColor(chosenColor, lowEntropyPixels[randomPixelIndex].x, lowEntropyPixels[randomPixelIndex].y);

	currentState = ComputationState::RUNNING;
	return;
}

boost::optional<Color> WFC::OutputWFCBitmapObject::getColor(int x, int y, unsigned short channels) {
	if(x < 0 || x >= width || y < 0 || y >= height) {
		return boost::none;
	}
	return pixels[BitmapPosition::positionToIndex(x, y, width)].getColor();
}

void WFC::OutputWFCBitmapObject::draw(Shader * shader, float scaleFactor) {
	BitmapObject::draw(shader, scaleFactor);
}

bool WFC::OutputWFCBitmapObject::isSet(const BitmapPosition & position) const {
	if(position.x < 0 || position.y < 0 || position.x >= width || position.y >= height) {
		return false;
	}
	unsigned int index = BitmapPosition::positionToIndex(position, width);
	if(index < pixels.size() && index >= 0) {
		return pixels[index].isSet();
	} else {
		return false;
	}
}

void WFC::OutputWFCBitmapObject::setColor(Color color, int x, int y) {
	// si assegna il colore, sia all'immagine vera sia a quella di visualizzazione
	// TODO far s� che il numero di canali si basi solo su quanto gi� si ha
	BitmapObject::setColor(image, x, y, width, height, 4, color);

	lastModifiedPixelPosition.x = x;
	lastModifiedPixelPosition.y = y;

	pixels[BitmapPosition::positionToIndex(lastModifiedPixelPosition, width)].setColor(color, image, width, height, time);


	// si ricalcolano i possibili colori per i pixel,
	// serve conoscere la posizione dell'ultimo pixel modificato e la dimensione del pattern

	// posizione minima condizionata dalla precedente osservazione
	BitmapPosition minAffectedPixelPosition = {
		lastModifiedPixelPosition.x - input.getTileSize(),
		lastModifiedPixelPosition.y - input.getTileSize() };
	BitmapPosition maxAffectedPixelPosition = {
		(lastModifiedPixelPosition.x + input.getTileSize() - 1),
		(lastModifiedPixelPosition.y + input.getTileSize() - 1)
	};

	minAffectedPixelPosition.x = Math::Clamp<int>(minAffectedPixelPosition.x, 0, width - 1);
	minAffectedPixelPosition.y = Math::Clamp<int>(minAffectedPixelPosition.y, 0, height - 1);
	maxAffectedPixelPosition.x = Math::Clamp<int>(maxAffectedPixelPosition.x, 0, width - 1);
	maxAffectedPixelPosition.y = Math::Clamp<int>(maxAffectedPixelPosition.y, 0, height - 1);

	recalculateSuperpositions(minAffectedPixelPosition, maxAffectedPixelPosition);
	//std::cout << "Color set at (" << lastModifiedPixelPosition.x << ", " << lastModifiedPixelPosition.y << ")" << std::endl;
}

void WFC::OutputWFCBitmapObject::clearArea(BitmapPosition startPoint, int size) {
	// area dove si eliminano tutti i pixel a prescindere
	BitmapPosition endPoint = BitmapPosition(startPoint.x + size / 2, startPoint.y + size / 2);

	endPoint.x = Math::Clamp(endPoint.x, 0, width - 1);
	endPoint.y = Math::Clamp(endPoint.y, 0, height - 1);
	startPoint.x -= size / 2;
	startPoint.y -= size / 2;
	startPoint.x = Math::Clamp(startPoint.x, 0, width - 1);
	startPoint.y = Math::Clamp(startPoint.y, 0, height - 1);

	for(int x = startPoint.x; x <= endPoint.x; x++) {
		for(int y = startPoint.y; y <= endPoint.y; y++) {
			resetAtPosition(BitmapPosition(x, y));
			if(getPixel(x, y).getVisualizedColor() == boost::none) {
				getPixel(x, y).setVisualizedColor(input.getAverageColor(), image, width, height);
				BitmapObject::setColor(image, x, y, width, height, 4, input.getAverageColor());
			} else {
				Color color = getPixel(x, y).getVisualizedColor().value();
				color.alpha = 100;
				BitmapObject::setColor(image, x, y, width, height, 4, color);
			}
		}
	}

	BitmapPosition recalculationStartPoint;
	BitmapPosition recalculationEndPoint;

	if(oldPixelsFirst) {
		// si eliminano i ((2*tilesize)^2 - tilesize^2) / 2 pixel pi� vecchi
		// preparazione punti di confine
		BitmapPosition secondStartPoint = BitmapPosition(startPoint.x - size / 2, startPoint.y - size / 2);
		BitmapPosition secondEndPoint = BitmapPosition(endPoint.x + size / 2, endPoint.y + size / 2);
		secondEndPoint.x = Math::Clamp(secondEndPoint.x, 0, width - 1);
		secondEndPoint.y = Math::Clamp(secondEndPoint.y, 0, height - 1);
		secondStartPoint.x = Math::Clamp(secondStartPoint.x, 0, width - 1);
		secondStartPoint.y = Math::Clamp(secondStartPoint.y, 0, height - 1);

		std::vector<int> olderPixels = std::vector<int>();
		for(int x = secondStartPoint.x; x <= secondEndPoint.x; x++) {
			for(int y = secondStartPoint.y; y <= secondEndPoint.y; y++) {
				int index = BitmapPosition::positionToIndex(x, y, width);
				if(!((x >= startPoint.x && x < endPoint.x) && (y >= startPoint.y && y < endPoint.y)) &&
					pixels[index].isSet()) {
					olderPixels.emplace_back(BitmapPosition::positionToIndex(x, y, width));
				}
			}
		}

		std::sort(olderPixels.begin(), olderPixels.end(), [&](int a, int b) { return pixels[a].getCreationTime() < pixels[b].getCreationTime(); });
		for(int i = 0; i < Math::Min<int>(((2 * size) * (2 * size) - size * size) / 2, olderPixels.size()); i++) {
			resetAtPosition(BitmapPosition::indexToPosition(olderPixels[i], width));
		}

		recalculationStartPoint = BitmapPosition(Math::Clamp(startPoint.x - input.getTileSize() * 2, 0, width - 1),
			Math::Clamp(startPoint.y - input.getTileSize() * 2, 0, height - 1));
		recalculationEndPoint = BitmapPosition(Math::Clamp(endPoint.x + input.getTileSize() * 2, 0, width - 1),
			Math::Clamp(endPoint.y + input.getTileSize() * 2, 0, height - 1));
	} else {
		recalculationStartPoint = BitmapPosition(Math::Clamp(startPoint.x - input.getTileSize(), 0, width - 1),
			Math::Clamp(startPoint.y - input.getTileSize(), 0, height - 1));
		recalculationEndPoint = BitmapPosition(Math::Clamp(endPoint.x + input.getTileSize(), 0, width - 1),
			Math::Clamp(endPoint.y + input.getTileSize(), 0, height - 1));
		
	}
	recalculateSuperpositions(recalculationStartPoint, recalculationEndPoint);
}

void WFC::OutputWFCBitmapObject::selectLowestEntropyPixels(std::vector<BitmapPosition> & emptyVector) {
	// initialization
	unsigned int minEntropy = std::numeric_limits<unsigned int>::max();

	// per ogni posizione dell'immagine di output (image), se questa � gi� definita
	// (ovvero se il valore corrispondente in set � true) si ignora, altrimenti si fa il reset
	for(int x = 0; x < width; x++) {
		for(int y = 0; y < height; y++) {
			BitmapPosition position(x, y);
			unsigned int imageIndex = BitmapPosition::positionToIndex(position, width);
			if(!isSet(position)) {
				unsigned int pixelEntropy = 0;

				// per ogni frequenza, si somma a pixelEntropy
				for(const auto& cf : pixels[BitmapPosition::positionToIndex(position, width)].superpositions) {
					pixelEntropy += cf.second;
				}

				if(pixelEntropy == minEntropy) {
					emptyVector.emplace_back(position);
				} else if(pixelEntropy < minEntropy) {
					minEntropy = pixelEntropy;
					emptyVector.clear();
					emptyVector.emplace_back(position);
				}
			}
		}
	}
}

void WFC::OutputWFCBitmapObject::resetAtPosition(BitmapPosition position) {
	getPixel(position.x, position.y).reset();
	image[BitmapPosition::positionToIndex(position, width) * 4 + 0] = 0;
	image[BitmapPosition::positionToIndex(position, width) * 4 + 1] = 0;
	image[BitmapPosition::positionToIndex(position, width) * 4 + 2] = 0;
	image[BitmapPosition::positionToIndex(position, width) * 4 + 3] = 0;
}

void WFC::OutputWFCBitmapObject::recalculateSuperpositions(BitmapPosition startPoint, BitmapPosition endPoint) {
	for(int x = startPoint.x; x <= endPoint.x; x++) {
		for(int y = startPoint.y; y <= endPoint.y; y++) {
			if(pixels[BitmapPosition::positionToIndex(x, y, width)].isSet()) {
				continue;
			}

			// si scoprono quali colori non vanno bene per questo pixel
			std::vector<Color> badColors;
			for(auto inputColor : input.getColors()) {

				bool badColorFound = false;
				for(int x2 = x - input.getTileSize() + 1; x2 <= x && !badColorFound; x2++) {
					for(int y2 = y - input.getTileSize() + 1; y2 <= y && !badColorFound; y2++) {
						if(!std::any_of(input.getPatterns().begin(), input.getPatterns().end(), [&](const std::pair<WFCPattern, unsigned int> & pattern) { return pattern.first.doesFit(BitmapPosition(x2, y2), *this, BitmapPosition(x, y), inputColor); })) {
							badColorFound = true;
							badColors.emplace_back(inputColor);
						}
					}
				}
			}

			// si riaggiornano le superposizioni
			pixels[BitmapPosition::positionToIndex(x, y, width)].superpositions.clear();

			for(auto pattern : input.getPatterns()) {
				for(int x2 = x - input.getTileSize() + 1; x2 <= x; x2++) {
					for(int y2 = y - input.getTileSize() + 1; y2 <= y; y2++) {
						BitmapPosition pixelPatternPosition(x - x2, y - y2);
						Color pixelPatternColor = pattern.first.getColor(pixelPatternPosition);
						int index = BitmapPosition::positionToIndex(x, y, width);
						if(std::find(badColors.begin(), badColors.end(), pixelPatternColor) == badColors.end() &&
							pattern.first.doesFit(BitmapPosition(x2, y2), *this)) {
							if(pixels[index].superpositions.find(pixelPatternColor) == pixels[index].superpositions.end()) {
								pixels[index].superpositions[pixelPatternColor] = 0;
							}
							pixels[index].superpositions[pixelPatternColor] += pattern.second;
						}
					}
				}
			}
		}
	}
}

WFC::WFCOutputPixel& WFC::OutputWFCBitmapObject::getPixel(const size_t x, const size_t y) {
	return pixels[BitmapPosition::positionToIndex(x, y, width)];
}

const Color WFC::OutputWFCBitmapObject::clearedAreaColor = { 255, 0, 0, 255 };