#pragma once
#include "WFCState.h"
#include "OutputWFCBitmapObject.h"
#include "InputWFCBitmapObject.h"
#include "Application.h"
#include "MeshObject.h"
#include <vector>
#include <memory>
#include <SDL.h>
#include <GL/glew.h>
#include <GL/GL.h>

class Renderer {
public:
	Renderer() = default;
	Renderer(class Application* app, float screenWidth, float screenHeight);

	bool initialize();
	void shutdown();
	void draw();

	void setWFCState(WFC::WFCState* state);

	// ci pensa lui a caricare la mesh
	class MeshObject* getMesh(const std::string& filename);
private:
	void createBitmapVerts();
	bool loadShaders();

	float screenWidth;
	float screenHeight;
	
	SDL_Window* window;
	SDL_GLContext context;

	std::vector<WFC::InputWFCBitmapObject*> bitmaps;
	std::vector<MeshObject> meshes;
	WFC::OutputWFCBitmapObject* outputImage;

	class VertexArray* bitmapVerts;
	class Shader* bitmapShader;
	class Application* app;
};

