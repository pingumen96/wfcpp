#include <map>
#include <memory>
#include "InputWFCBitmapObject.h"
#include "WFCPattern.h"
#include "Texture.h"
#include "Shader.h"
#include "Math.h"
#include "Color.h"


WFC::InputWFCBitmapObject::InputWFCBitmapObject(class Application* app, const unsigned short tileSize, const std::string& filename, const bool evaluateRotations, const bool evaluateXReflection, const bool evaluateYReflection) :
	BitmapObject(app),
	tileSize(tileSize),
	evaluateXReflection(evaluateXReflection),
	evaluateYReflection(evaluateYReflection),
	evaluateRotations(evaluateRotations) {

	// inizializzazioni utili
	patterns = std::map<WFCPattern, unsigned int>();
	colorsFrequency = std::map<Color, unsigned int>();

	// si salva la texture (utile per OpenGL)
	Texture* tex = new Texture();
	tex->load(filename);
	setTexture(tex);

	// si salvano tutti i colori, ciclando nell'immagine e prendendo tutti i canali
	std::vector<uint8_t> image = texture->getImage();

	// si cicla nell'immagine
	int horizontalPatterns = texture->getWidth() - tileSize + 1;
	int verticalPatterns = texture->getHeight() - tileSize + 1;
	for(int i = 0; i < verticalPatterns; i++) {
		for(int j = 0; j < horizontalPatterns; j++) {
			WFCPattern pattern = WFCPattern(this, tileSize, BitmapPosition(j, i));
			addPattern(pattern);

			// aggiunta eventuali pattern ruotati
			if(this->evaluateRotations) {
				std::vector<WFCPattern> rotations;
				pattern.getRotations(pattern, rotations);
				for(auto p : rotations) {
					addPattern(p);
				}
			}

			// aggiunta eventuali pattern riflessi
			if(this->evaluateXReflection) {
				addPattern(pattern.getReflection(pattern, Math::Axis::X));
			}

			if(this->evaluateYReflection) {
				addPattern(pattern.getReflection(pattern, Math::Axis::Y));
			}
		}
	}

	// calcolo del colore medio
	unsigned int redAccumulator = 0, greenAccumulator = 0, blueAccumulator = 0;
	for(auto color : colorsFrequency) {
		redAccumulator += color.first.red;
		greenAccumulator += color.first.green;
		blueAccumulator += color.first.blue;
	}
	averageColor.red = static_cast<uint8_t>(redAccumulator / colorsFrequency.size());
	averageColor.green = static_cast<uint8_t>(greenAccumulator / colorsFrequency.size());
	averageColor.blue = static_cast<uint8_t>(blueAccumulator / colorsFrequency.size());
	averageColor.alpha = 255;
}


WFC::InputWFCBitmapObject::~InputWFCBitmapObject() {
}

void WFC::InputWFCBitmapObject::drawTiles(Shader* shader) {
	// si disegnano i tiles
	/*if(texture) {
		Matrix4 scaleMat = Matrix4::CreateScale(
			static_cast<float>(tileSize) * 20.0f,
			static_cast<float>(tileSize) * 20.0f,
			1.0f);

		Matrix4 world = scaleMat * worldTransform;

		shader->setMatrixUniform("uWorldTransform", world);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, patterns[4].id);
		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, nullptr);
	}*/
}

void WFC::InputWFCBitmapObject::setTexture(Texture* texture) {
	BitmapObject::setTexture(texture);

	// debug
	std::cout << "There are " << texture->getChannels() << " channels" << std::endl;
}

std::vector<Color> WFC::InputWFCBitmapObject::getColors() {
	std::vector<Color> colors;

	for(auto cf : colorsFrequency) {
		colors.emplace_back(cf.first);
	}

	return colors;
}

void WFC::InputWFCBitmapObject::addPattern(WFCPattern pattern) {
	// se l'oggetto non esiste lo si inizializza, altrimenti si incrementa il contatore
	if(patterns.find(pattern) == patterns.end()) {
		patterns[pattern] = 1;
	} else {
		patterns[pattern]++;
	}

	// per ogni colore e frequenza nel pattern, si aggiorna colorsFrequency
	for(auto c : pattern.getColors()) {
		if(colorsFrequency.find(c) == colorsFrequency.end()) {
			colorsFrequency[c] = pattern.getColorFrequency(c);
		} else {
			colorsFrequency[c] += pattern.getColorFrequency(c);
		}
	}
}
