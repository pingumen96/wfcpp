#pragma once
#include "MeshObject.h"
class TriMeshObject :
	public MeshObject {
public:
	TriMeshObject() = delete;
	TriMeshObject(class Application* app);
};

