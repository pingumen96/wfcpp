#include "BitmapObject.h"
#include "Texture.h"
#include "Color.h"
#include "Shader.h"
#include <vector>
#include <boost/optional.hpp>

using namespace WFC;

BitmapObject::BitmapObject(class Application* app) :
	Object(app),
	texture(nullptr){
}


BitmapObject::~BitmapObject() {
}

void BitmapObject::draw(Shader* shader) {
	draw(shader, 1.0f);
}

void BitmapObject::draw(Shader* shader, float scaleFactor) {
	if(texture) {
		Matrix4 scaleMat = Matrix4::CreateScale(
			static_cast<float>(texture->getWidth()) * scaleFactor,
			static_cast<float>(texture->getHeight()) * scaleFactor,
			1.0f);

		Matrix4 world = scaleMat * worldTransform;

		shader->setMatrixUniform("uWorldTransform", world);
		texture->setActive();

		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, nullptr);
	}
}

void BitmapObject::setTexture(Texture* texture) {
	this->texture = texture;
}

unsigned short BitmapObject::getChannels() {
	return texture->getChannels();
}

boost::optional<Color> BitmapObject::getColor(int x, int y, unsigned short channels) const {
	// TODO verificare funzionamento
	return discoverColor(texture->getImage(), x, y, texture->getWidth(), channels);
}



void BitmapObject::setColor(std::vector<uint8_t>& image, int x, int y, unsigned int imgWidth, unsigned int imgHeight, unsigned short channels, Color color) {
	image[y * imgWidth * channels + (x % imgWidth) * channels + 0] = color.red;
	image[y * imgWidth * channels + (x % imgWidth) * channels + 1] = color.green;
	image[y * imgWidth * channels + (x % imgWidth) * channels + 2] = color.blue;

	if(channels == 4) {
		image[y * imgWidth * channels + (x % imgWidth) * channels + 3] = color.alpha;
	}

	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, imgWidth, imgHeight, GL_RGBA, GL_UNSIGNED_BYTE, image.data());
}

Color BitmapObject::discoverColor(const std::vector<uint8_t>& image, const int x, const int y, unsigned int imgWidth, unsigned short channels) {
	int index = (x * imgWidth) * channels + (y % imgWidth) * channels;
	if(channels == 4) {
		return Color(image[index + 0],
			image[index + 1],
			image[index + 2],
			image[index + 3]);
	} else {
		return Color(image[index + 0],
			image[index + 1],
			image[index + 2],
			255);
	}
}

void BitmapObject::fillWithColor(std::vector<uint8_t>& image, size_t imgWidth, size_t imgHeight, unsigned short channels, Color color) {
	for(unsigned int i = 0; i < imgHeight * imgWidth; i++) {
		image[i * channels + 0] = color.red;
		image[i * channels + 1] = color.green;
		image[i * channels + 2] = color.blue;

		if(channels == 4) {
			image[i * channels + 3] = color.alpha;
		}
	}
}