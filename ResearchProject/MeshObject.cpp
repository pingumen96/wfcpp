#include <iostream>
#include <fstream>
#include <sstream>
#include "MeshObject.h"
#include "Renderer.h"



MeshObject::MeshObject(class Application* app) :
	Object(app),
	vertexArray(nullptr){
}

bool MeshObject::load(const std::string & filename, Application* app) {
	// code adapted from: https://en.wikibooks.org/wiki/OpenGL_Programming/Modern_OpenGL_Tutorial_Load_OBJ
	std::ifstream in(filename.c_str(), std::ios::in);
	if(!in) {
		std::cerr << "Cannot open " << filename << std::endl; exit(1);
	}

	std::string line;
	while(std::getline(in, line)) {
		if(line.substr(0, 2) == "v ") {
			std::istringstream s(line.substr(2));
			Vector4 v;
			s >> v.x;
			s >> v.y;
			s >> v.z;
			v.w = 1.0f;
			vertices.push_back(v);
		} else if(line.substr(0, 2) == "f ") {
			std::istringstream s(line.substr(2));
			GLushort a, b, c;
			s >> a;
			s >> b;
			s >> c;
			a--;
			b--; 
			c--;
			elements.push_back(a);
			elements.push_back(b);
			elements.push_back(c);
		} else if(line[0] == '#') {
			/* ignoring this line */
		} else {
			/* ignoring this line */
		}
	}

	normals.resize(vertices.size(), Vector3(0.0f, 0.0f, 0.0f));
	for(size_t i = 0; i < elements.size(); i += 3) {
		GLushort ia = elements[i];
		GLushort ib = elements[i + 1];
		GLushort ic = elements[i + 2];
		Vector3 normal = Vector3::Normalize(Vector3::Cross(
			Vector3(vertices[ib].x, vertices[ib].y, vertices[ib].z) - Vector3(vertices[ia].x, vertices[ia].y, vertices[ia].z),
			Vector3(vertices[ic].x, vertices[ic].y, vertices[ic].z) - Vector3(vertices[ia].x, vertices[ia].y, vertices[ia].z)));
		normals[ia] = normals[ib] = normals[ic] = normal;
	}
}

void MeshObject::unload() {
}
