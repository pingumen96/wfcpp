#include "Object.h"
#include "Application.h"
#include <memory>



Object::Object(Application* app) : app(app),
	scale(1.0f){
}


void Object::computeWorldTransform() {
	recomputeTransform = false;

	worldTransform = Matrix4::CreateScale(scale);
	worldTransform *= Matrix4::CreateFromQuaternion(rotation);
	worldTransform *= Matrix4::CreateTranslation(position);
}
