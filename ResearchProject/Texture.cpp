#include "Texture.h"



Texture::Texture() : textureID(0), width(0), height(0) {
}


Texture::~Texture() {
}

bool Texture::load(const std::string & filename) {
	this->filename = filename;
	channels = 0;

	uint8_t* bmp = SOIL_load_image(this->filename.c_str(), &width, &height, &channels, SOIL_LOAD_AUTO);
	return load(bmp, width, height, channels);

}

bool Texture::load(const uint8_t * bmp, int width, int height, int channels) {
	this->width = width;
	this->height = height;
	this->channels = channels;
	if(bmp == nullptr) {
		SDL_Log("SOIL failed to load image %s: %s", this->filename.c_str(), SOIL_last_result());
		return false;
	}

	image.resize(width * height * channels);
	for(unsigned int i = 0; i < image.size(); i++) {
		image[i] = bmp[i];
	}

	int format = GL_RGB;

	if(channels == 4) {
		format = GL_RGBA;
	}

	glGenTextures(1, &textureID);
	glBindTexture(GL_TEXTURE_2D, textureID);

	glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, image.data());

	//SOIL_free_image_data(image);

	glGenerateMipmap(GL_TEXTURE_2D);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	if(GLEW_EXT_texture_filter_anisotropic) {
		GLfloat largest;
		glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &largest);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, largest);
	}

	return true;
}

void Texture::unload() {
	glDeleteTextures(1, &textureID);
}

void Texture::createForRendering(int width, int height, unsigned int format, uint8_t* pixels) {
	this->width = width;
	this->height = height;

	glGenTextures(1, &textureID);
	glBindTexture(GL_TEXTURE_2D, textureID);
	glTexImage2D(GL_TEXTURE_2D, 0, format, this->width, this->height, 0, format, GL_FLOAT, pixels);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
}

void Texture::setActive(int index) {
	glActiveTexture(GL_TEXTURE0 + index);
	glBindTexture(GL_TEXTURE_2D, textureID);
}
