#pragma once
#include <stdint.h>
#include <tuple>
#include <map>
#include <GL/glew.h>
#include <GL/GL.h>
#include <boost/functional/hash.hpp>
#include "BitmapObject.h"

namespace WFC {
	// tutt'altro che safe, piano piano andrebbe migliorata
	class WFCPattern {
	public:
		static const int CHANNELS = 4;

		WFCPattern(); // sta cosa va corretta, va resa inutilizzabile
		WFCPattern(class InputWFCBitmapObject* owner, unsigned short size, BitmapPosition position);
		WFCPattern(class InputWFCBitmapObject* owner, unsigned short size, std::vector<uint8_t>& bmp, std::vector<std::vector<uint8_t>>& pixels);

		// il seguente attributo � volutamente public per immediatezza di implementazione, dunque occhio
		GLuint id;

		void setSize(unsigned short size) { this->edgeSize = size; }
		unsigned int getColorFrequency(Color& index) { return colorsFrequencies[index]; }
		std::vector<Color> getColors() const;
		Color getColor(BitmapPosition position) const;
		Color getColor(int x, int y) const;
		unsigned short getSize() const { return edgeSize; }
		const std::vector<std::vector<uint8_t>>& getPixels() { return pixels; }

		bool doesFit(struct BitmapPosition minCorner, class OutputWFCBitmapObject& output, boost::optional<struct BitmapPosition> position = boost::none, boost::optional<Color&> color = boost::none) const;

		void initializeGL();
		void getRotations(WFCPattern pattern, std::vector<WFCPattern>& container);
		WFCPattern getReflection(WFCPattern pattern, Math::Axis axis);

		inline friend bool operator<(const WFCPattern& l, const WFCPattern& r) {
			return l.id < r.id;
		}

		inline friend bool operator==(const WFCPattern& l, const WFCPattern& r) {
			return l.pixels == r.pixels;
		}

		inline friend bool operator!=(const WFCPattern& l, const WFCPattern& r) {
			return !(l.pixels == r.pixels);
		}
	private:
		unsigned short edgeSize;
		std::map<Color, unsigned int> colorsFrequencies;
		std::vector<std::vector<uint8_t>> pixels;
		std::vector<uint8_t> bmp;
		class InputWFCBitmapObject* owner; // TODO trovare un modo per poter utilizzare reference invece di un puntatore
	};

}

