#include <SDL.h>
#include <memory>
#include "Application.h"
#include "Random.h"
#include "Renderer.h"
#include "WFCState.h"
#include "InputWFCBitmapObject.h"
#include "OutputWFCBitmapObject.h"



Application::Application() : renderer(nullptr),
	isRunning(true),
	isPaused(false),
	canPause(true),
	wfcState(nullptr),
	finished(false),
	random(std::make_unique<Random>())
{
}

bool Application::initialize() {
	if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO)) {
		SDL_Log("Impossibile inizializzare SDL: %s", SDL_GetError());
		return false;
	}

	renderer = new Renderer(this, 600.0f, 600.0f);
	if(!renderer->initialize()) {
		SDL_Log("Problemi nell'inizializzare il renderer");
		delete renderer;
		renderer = nullptr;
		return false;
	}

	loadData(); // metodo temporaneo

	return true;
}

void Application::shutdown() {
	if(renderer) {
		renderer->shutdown();
	}

	SDL_Quit();
}

void Application::generateOutput() {
	renderer->draw();
}

void Application::runLoop() {
	while(isRunning && !finished) {
		processInput();
		if(!isPaused && canPause) {
			updateObjects();
		}
		generateOutput();
	}

	wfcState->getOutputBitmap()->saveImage("image.bmp");
}

void Application::processInput() {
	SDL_Event event;

	while(SDL_PollEvent(&event)) {
		switch(event.type) {
		case SDL_QUIT:
			isRunning = false;
			break;
		case SDL_KEYDOWN:
			if(!event.key.repeat) {
				handleKeyPress(event.key.keysym.sym);
			}
			break;
		}
	}
}

void Application::updateObjects() {
	WFC::OutputWFCBitmapObject::ComputationState currentState = wfcState->getOutputBitmap()->getCurrentState();

	switch(currentState) {
	case WFC::OutputWFCBitmapObject::ComputationState::CONTRADICTION:
		if(canPause) {
			isPaused = true;
		}
		wfcState->getOutputBitmap()->observe();
		break;
	case WFC::OutputWFCBitmapObject::ComputationState::FINISHED:
		finished = true;
		return;
	case WFC::OutputWFCBitmapObject::ComputationState::RUNNING:
		wfcState->getOutputBitmap()->observe();
		break;
	}
}

// qua dentro avviene hardcoding per caricare ci� che si vuole
void Application::loadData() {
	int tilesize = 3;
	int clearsize = tilesize + 1;
	WFC::InputWFCBitmapObject* input = new WFC::InputWFCBitmapObject(this, tilesize, "./Samples/Skyline.png", false, false, false);
	WFC::OutputWFCBitmapObject* output = new WFC::OutputWFCBitmapObject(this, *input, 100, 100, clearsize, false, false, true, true);
	
	unsigned int numberOfIterations = 0;

	wfcState = new WFC::WFCState(input, output);
	renderer->setWFCState(wfcState);
}

void Application::handleKeyPress(int key) {
	switch(key) {
	case ' ':
		isPaused = !isPaused;
		break;
	case 'p':
		canPause = !canPause;
		break;
	}
}
