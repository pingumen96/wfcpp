#include "Color.h"
#include <map>

Color Color::averageColor(const std::map<Color, unsigned int>& colors) {
	// Acc stands for "accumulator"
	unsigned long redAcc = 0, greenAcc = 0, blueAcc = 0, alphaAcc = 0, size = 0;
	
	for(const auto& c : colors) {
		redAcc += c.first.red * c.second;
		greenAcc += c.first.green * c.second;
		blueAcc += c.first.blue * c.second;
		alphaAcc += c.first.alpha * c.second;
		size += c.second;
	}

	Color result(static_cast<uint8_t>(redAcc / size), static_cast<uint8_t>(greenAcc / size),
		static_cast<uint8_t>(blueAcc / size), static_cast<uint8_t>(alphaAcc / size));
	
	return result;
}
